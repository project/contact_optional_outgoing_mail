<?php

/**
 * @file
 * Contact optional outgoing mail module file.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
function contact_optional_outgoing_mail_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id !== 'contact_form_add_form' && $form_id !== 'contact_form_edit_form') {
    return;
  }

  // Make 'recipients' optional.
  $form['recipients']['#required'] = FALSE;

  // Add two validators. See the function definitions below.
  array_unshift($form['#validate'], '_contact_optional_outgoing_mail_form_validate');
  $form['#validate'][] = '_contact_optional_outgoing_mail_form_validate_after';
}

/**
 * {@inheritdoc}
 */
function _contact_optional_outgoing_mail_form_validate(array &$form, FormStateInterface $form_state) {
  if (empty($form_state->getValue('recipients'))) {
    // If no recipients are entered, we'll set a dummy value, so we will pass
    // Drupal\contact\ContactFormEditForm:validateForm validation.
    $form_state->setValue('recipients', 'contact-optional-outgoing-mail@contact-optional-outgoing-mail.com');
  }
}

/**
 * {@inheritdoc}
 */
function _contact_optional_outgoing_mail_form_validate_after(array &$form, FormStateInterface $form_state) {
  $recipients = $form_state->getValue('recipients');
  if (count($recipients) === 1 && $recipients[0] === 'contact-optional-outgoing-mail@contact-optional-outgoing-mail.com') {
    // If our dummy value is set as the recipient, we'll remove it.
    $form_state->setValue('recipients', []);
  }
}

/**
 * Implements hook_mail_alter().
 */
function contact_optional_outgoing_mail_mail_alter(&$message) {
  // If an email from the contact module with no recipients, disable sending.
  $contact_form_ids = [
    'contact_page_autoreply',
    'contact_page_mail',
  ];

  if (in_array($message['id'], $contact_form_ids) && empty($message['to'])) {
    /**
     * @var \Drupal\contact\Entity\Message $contact_message
     */
    $contact_message = $message['params']['contact_message'];

    // Determine if there is an option selected in a options_email field.
    $hasContactOptionsEmailRecipient = FALSE;
    foreach ($contact_message->getFields() as $field) {
      if ($field->getFieldDefinition()
        ->getType() === 'contact_storage_options_email') {
        foreach ($field as $item) {
          $label = $item->value;
          $email = $item->getFieldDefinition()
            ->getSetting('allowed_values')[$label]['emails'];
          if (!empty($email)) {
            $hasContactOptionsEmailRecipient = TRUE;
          }
        }
      }
    }

    if (!$hasContactOptionsEmailRecipient) {
      $message['send'] = FALSE;
    }
  }

}
